/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1577976456_3_
#define _BUR_1577976456_3_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef enum MainStep_enum
{	WAIT,
	GO_UP,
	GO_DOWN,
	ERROR
} MainStep_enum;

typedef struct LiftStatus_typ
{	plcbit Error;
} LiftStatus_typ;

typedef struct LiftCommands_typ
{	plcbit Up;
	plcbit Down;
	plcbit Reset;
} LiftCommands_typ;

typedef struct LiftInterface_typ
{	struct LiftStatus_typ Status;
	struct LiftCommands_typ Cmds;
} LiftInterface_typ;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1577976456_3_ */

