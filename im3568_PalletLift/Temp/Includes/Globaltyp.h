/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1577976456_1_
#define _BUR_1577976456_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct ActuatorInputs_typ
{	plcbit MinusWarn;
	plcbit MinusLimit;
	plcbit PlusWarn;
	plcbit PlusLimit;
	plcbit Clamped;
	plcbit Unclampled;
	plcbit SafetyStatus;
	plcbit Active;
	plcbit Fault;
} ActuatorInputs_typ;

typedef struct DigitalInputs_typ
{	struct ActuatorInputs_typ RightActuator;
	struct ActuatorInputs_typ LeftActuator;
	plcbit PalletDetect;
} DigitalInputs_typ;

typedef struct ActuatorOutputs_typ
{	plcbit DI1_StartStop;
	plcbit DI2_ForwardReverse;
	plcbit DI3_SpeedSelection;
	plcbit DI4_SpeedSelection;
	plcbit Unclamp;
	plcbit Clamp;
} ActuatorOutputs_typ;

typedef struct StackLight_typ
{	plcbit RedLight;
	plcbit YellowLight;
	plcbit GreenLight;
} StackLight_typ;

typedef struct DigitalOutputs_typ
{	struct ActuatorOutputs_typ RightActuator;
	struct ActuatorOutputs_typ LeftActuator;
	struct StackLight_typ Lights;
	plcbit Enable;
	plcbit FaultReset;
	plcbit Mute1;
	plcbit Mute2;
} DigitalOutputs_typ;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1577976456_1_ */

