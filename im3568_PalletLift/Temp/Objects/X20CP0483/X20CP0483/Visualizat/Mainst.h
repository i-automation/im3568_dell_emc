#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_Inputs_typ
#define __AS__TYPE_Inputs_typ
typedef struct Inputs_typ
{	plcwstring IN1[11];
	plcwstring IN2[11];
	plcwstring IN3[11];
	plcwstring IN4[11];
	plcwstring IN5[11];
	plcwstring IN6[11];
	plcwstring IN7[11];
	plcwstring IN8[11];
	plcwstring IN9[11];
	plcwstring IN10[11];
	plcwstring IN11[11];
	plcwstring IN12[11];
	plcwstring IN13[11];
	plcwstring IN14[11];
	plcwstring IN15[11];
	plcwstring IN16[11];
	plcwstring IN17[11];
	plcwstring IN18[11];
	plcwstring IN19[11];
	plcwstring IN20[11];
	plcwstring IN21[11];
	plcwstring IN22[11];
	plcwstring IN23[11];
	plcwstring IN24[11];
} Inputs_typ;
#endif

#ifndef __AS__TYPE_Outputs_typ
#define __AS__TYPE_Outputs_typ
typedef struct Outputs_typ
{	plcwstring OUT1[11];
	plcwstring OUT2[11];
	plcwstring OUT3[11];
	plcwstring OUT4[11];
	plcwstring OUT5[11];
	plcwstring OUT6[11];
	plcwstring OUT7[11];
	plcwstring OUT8[11];
	plcwstring OUT9[11];
	plcwstring OUT10[11];
	plcwstring OUT11[11];
	plcwstring OUT12[11];
	plcwstring OUT13[11];
	plcwstring OUT14[11];
	plcwstring OUT15[11];
	plcwstring OUT16[11];
	plcwstring OUT17[11];
	plcwstring OUT18[11];
	plcwstring OUT19[11];
	plcwstring OUT20[11];
	plcwstring OUT21[11];
	plcwstring OUT22[11];
	plcwstring OUT23[11];
	plcwstring OUT24[11];
} Outputs_typ;
#endif

#ifndef __AS__TYPE_InputsOutputs_typ
#define __AS__TYPE_InputsOutputs_typ
typedef struct InputsOutputs_typ
{	Inputs_typ In;
	Outputs_typ Out;
} InputsOutputs_typ;
#endif

#ifndef __AS__TYPE_mappView_typ
#define __AS__TYPE_mappView_typ
typedef struct mappView_typ
{	plcwstring PlusLimit[11];
	plcwstring PlusWarn[11];
	plcwstring MinusWarn[11];
	plcwstring MinusLimit[11];
	plcbit Clamped;
	plcbit Unclampled;
	unsigned short Counter;
	plcwstring RightSafeStatus[11];
	plcwstring RightVFDStatus[11];
	plcwstring RightActive[11];
	plcwstring LeftSafeStatus[11];
	plcwstring LeftVFDStatus[11];
	plcwstring LeftActive[11];
	plcwstring PalletPresent[11];
	InputsOutputs_typ IO;
} mappView_typ;
#endif

#ifndef __AS__TYPE_ActuatorInputs_typ
#define __AS__TYPE_ActuatorInputs_typ
typedef struct ActuatorInputs_typ
{	plcbit MinusWarn;
	plcbit MinusLimit;
	plcbit PlusWarn;
	plcbit PlusLimit;
	plcbit Clamped;
	plcbit Unclampled;
	plcbit SafetyStatus;
	plcbit Active;
	plcbit Fault;
} ActuatorInputs_typ;
#endif

#ifndef __AS__TYPE_DigitalInputs_typ
#define __AS__TYPE_DigitalInputs_typ
typedef struct DigitalInputs_typ
{	ActuatorInputs_typ RightActuator;
	ActuatorInputs_typ LeftActuator;
	plcbit PalletDetect;
} DigitalInputs_typ;
#endif

#ifndef __AS__TYPE_ActuatorOutputs_typ
#define __AS__TYPE_ActuatorOutputs_typ
typedef struct ActuatorOutputs_typ
{	plcbit DI1_StartStop;
	plcbit DI2_ForwardReverse;
	plcbit DI3_SpeedSelection;
	plcbit DI4_SpeedSelection;
	plcbit Unclamp;
	plcbit Clamp;
} ActuatorOutputs_typ;
#endif

#ifndef __AS__TYPE_StackLight_typ
#define __AS__TYPE_StackLight_typ
typedef struct StackLight_typ
{	plcbit RedLight;
	plcbit YellowLight;
	plcbit GreenLight;
} StackLight_typ;
#endif

#ifndef __AS__TYPE_DigitalOutputs_typ
#define __AS__TYPE_DigitalOutputs_typ
typedef struct DigitalOutputs_typ
{	ActuatorOutputs_typ RightActuator;
	ActuatorOutputs_typ LeftActuator;
	StackLight_typ Lights;
	plcbit Enable;
	plcbit FaultReset;
	plcbit Mute1;
	plcbit Mute2;
} DigitalOutputs_typ;
#endif

_BUR_LOCAL mappView_typ Vis;
_GLOBAL DigitalInputs_typ Inputs;
_GLOBAL DigitalOutputs_typ Outputs;
