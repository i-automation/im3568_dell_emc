#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Visualizat/Mainst.h"
#line 1 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.nodebug"
#line 2 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{
}}
#line 3 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.nodebug"
#line 5 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{
(Vis.Counter=(Vis.Counter+1));

if(Inputs.LeftActuator.PlusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PlusLimit; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PlusLimit; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.LeftActuator.PlusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PlusWarn; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PlusWarn; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.LeftActuator.MinusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.MinusWarn; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.MinusWarn; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.LeftActuator.MinusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.MinusLimit; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.MinusLimit; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Unclampled){
(Vis.Unclampled=1);
}else{
(Vis.Unclampled=0);
}
if(Inputs.LeftActuator.Clamped){
(Vis.Clamped=1);
}else{
(Vis.Clamped=0);
}


if(Inputs.LeftActuator.SafetyStatus){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftSafeStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftSafeStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){82,101,100,76,69,68,0}); for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.RightActuator.SafetyStatus){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightSafeStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightSafeStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){82,101,100,76,69,68,0}); for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Fault){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftVFDStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftVFDStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){82,101,100,76,69,68,0}); for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.RightActuator.Fault){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightVFDStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightVFDStatus; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){82,101,100,76,69,68,0}); for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Active){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftActive; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.LeftActive; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
if(Inputs.RightActuator.Active){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightActive; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.RightActive; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.PalletDetect){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PalletPresent; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.PalletPresent; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}






if(Inputs.LeftActuator.MinusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN1; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN1; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.MinusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN2; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN2; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.PlusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN3; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN3; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.PlusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN4; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN4; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Clamped){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN5; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN5; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.SafetyStatus){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN6; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN6; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.SafetyStatus){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN7; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN7; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Unclampled){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN8; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN8; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.PalletDetect){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN9; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN9; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Active){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN10; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN10; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.LeftActuator.Fault){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN11; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN11; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN12; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


if(Inputs.RightActuator.MinusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN13; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN13; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.MinusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN14; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN14; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.PlusWarn){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN15; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN15; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.PlusLimit){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN16; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN16; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.Clamped){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN17; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN17; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.Unclampled){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN18; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN18; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN19; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN20; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN21; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

if(Inputs.RightActuator.Active){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN22; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN22; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Inputs.RightActuator.Fault){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN23; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN23; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.In.IN24; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};



if(Outputs.LeftActuator.DI1_StartStop){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT1; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT1; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.LeftActuator.DI2_ForwardReverse){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT2; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT2; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.LeftActuator.DI3_SpeedSelection){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT3; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT3; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.LeftActuator.DI4_SpeedSelection){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT4; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT4; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.LeftActuator.Unclamp){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT5; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT5; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.LeftActuator.Clamp){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT6; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT6; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT7; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT8; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

if(Outputs.Enable){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT9; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT9; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.FaultReset){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT10; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT10; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.Mute1){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT11; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT11; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.Mute2){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT12; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT12; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}


if(Outputs.RightActuator.DI1_StartStop){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT13; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT13; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.RightActuator.DI2_ForwardReverse){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT14; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT14; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.RightActuator.DI3_SpeedSelection){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT15; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT15; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.RightActuator.DI4_SpeedSelection){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT16; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT16; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.RightActuator.Unclamp){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT17; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT17; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.RightActuator.Clamp){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT18; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT18; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT19; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT20; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT21; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

if(Outputs.Lights.RedLight){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT22; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT22; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.Lights.YellowLight){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT23; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT23; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

if(Outputs.Lights.GreenLight){
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT24; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,101,110,76,69,68,0}); for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcwstring* zzLValue=(plcwstring*)Vis.IO.Out.OUT24; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){71,114,101,121,76,69,68,0}); for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
}imp1_end50_0:;}
#line 336 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.nodebug"
#line 338 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.st"
void _EXIT __BUR__ENTRY_EXIT_FUNCT__(void){{
}}
#line 339 "C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Visualization/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Visualization/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Visualizat/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Visualizat/Main.st.c\\\" \\\"C:/repos/DellEMC/im3568_PalletLift/Logical/Visualization/Main.st\\\"\\n\"");
__asm__(".previous");
