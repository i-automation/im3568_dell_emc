#ifndef __AS__TYPE_Inputs_typ
#define __AS__TYPE_Inputs_typ
typedef struct Inputs_typ
{	plcwstring IN1[11];
	plcwstring IN2[11];
	plcwstring IN3[11];
	plcwstring IN4[11];
	plcwstring IN5[11];
	plcwstring IN6[11];
	plcwstring IN7[11];
	plcwstring IN8[11];
	plcwstring IN9[11];
	plcwstring IN10[11];
	plcwstring IN11[11];
	plcwstring IN12[11];
	plcwstring IN13[11];
	plcwstring IN14[11];
	plcwstring IN15[11];
	plcwstring IN16[11];
	plcwstring IN17[11];
	plcwstring IN18[11];
	plcwstring IN19[11];
	plcwstring IN20[11];
	plcwstring IN21[11];
	plcwstring IN22[11];
	plcwstring IN23[11];
	plcwstring IN24[11];
} Inputs_typ;
#endif

#ifndef __AS__TYPE_Outputs_typ
#define __AS__TYPE_Outputs_typ
typedef struct Outputs_typ
{	plcwstring OUT1[11];
	plcwstring OUT2[11];
	plcwstring OUT3[11];
	plcwstring OUT4[11];
	plcwstring OUT5[11];
	plcwstring OUT6[11];
	plcwstring OUT7[11];
	plcwstring OUT8[11];
	plcwstring OUT9[11];
	plcwstring OUT10[11];
	plcwstring OUT11[11];
	plcwstring OUT12[11];
	plcwstring OUT13[11];
	plcwstring OUT14[11];
	plcwstring OUT15[11];
	plcwstring OUT16[11];
	plcwstring OUT17[11];
	plcwstring OUT18[11];
	plcwstring OUT19[11];
	plcwstring OUT20[11];
	plcwstring OUT21[11];
	plcwstring OUT22[11];
	plcwstring OUT23[11];
	plcwstring OUT24[11];
} Outputs_typ;
#endif

#ifndef __AS__TYPE_InputsOutputs_typ
#define __AS__TYPE_InputsOutputs_typ
typedef struct InputsOutputs_typ
{	Inputs_typ In;
	Outputs_typ Out;
} InputsOutputs_typ;
#endif

#ifndef __AS__TYPE_mappView_typ
#define __AS__TYPE_mappView_typ
typedef struct mappView_typ
{	plcwstring PlusLimit[11];
	plcwstring PlusWarn[11];
	plcwstring MinusWarn[11];
	plcwstring MinusLimit[11];
	plcbit Clamped;
	plcbit Unclampled;
	unsigned short Counter;
	plcwstring RightSafeStatus[11];
	plcwstring RightVFDStatus[11];
	plcwstring RightActive[11];
	plcwstring LeftSafeStatus[11];
	plcwstring LeftVFDStatus[11];
	plcwstring LeftActive[11];
	plcwstring PalletPresent[11];
	InputsOutputs_typ IO;
} mappView_typ;
#endif

_BUR_LOCAL mappView_typ Vis;
