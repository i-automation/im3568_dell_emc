define(['system/widgets/NumPad/NumPad', 'brease/decorators/LanguageDependency'], 
    function (SuperClass, languageDependency) {

        'use strict';

        /**
        * @class widgets.__WIDGET_LIBRARY__.__WIDGET_NAME__
        * #Description
        *   
        * @breaseNote
        * @extends system.widgets.NumPad
        *
        * @iatMeta category:Category
        * Keyboards
        * @iatMeta description:short
        * Custom keyboard
        * @iatMeta description:de
        * Custom keyboard
        * @iatMeta description:en
        * Custom keyboard
        */

        var defaultSettings = {
                html: 'widgets/__WIDGET_LIBRARY__/__WIDGET_NAME__/__WIDGET_NAME__.html',
                stylePrefix: 'widgets___WIDGET_LIBRARY_____WIDGET_NAME__',
                width: __WIDTH__,
                height: __HEIGHT__,
                showCloseButton: false
            },
            WidgetClass = SuperClass.extend(function __WIDGET_NAME__(elem, options, deferredInit, inherited) {
                if (inherited === true) {
                    SuperClass.call(this, null, null, true, true);
                    this.loadHTML();
                } else {
                    if (instance === undefined) {
                        SuperClass.call(this, null, null, true, true);
                        this.loadHTML();
                        instance = this;
                    } else {
                        return instance;
                    }
                }
            }, defaultSettings),
            instance,

            p = WidgetClass.prototype;
    
        p.dispose = function () {
            SuperClass.prototype.dispose.apply(this, arguments);
            instance = undefined;
        };

        p.readyHandler = function () {
            this.setTexts();
            SuperClass.prototype.readyHandler.apply(this, arguments);
        };

        p.langChangeHandler = function (e) {
            this.setTexts();
        };

        p.setTexts = function () {
            var widget = this;
            if (!this.arText) {
                this.arText = [];
                this.el.find('[data-display]').each(function () {
                    var $el = $(this),
                        display = $el.data('display'),
                        $textEl = ($el.hasClass('display')) ? $el : $el.find('.display');
                    if (brease.language.isKey(display) === true) {
                        var item = {
                            key: brease.language.parseKey(display),
                            textEl: $textEl
                        };
                        widget.arText.push(item);
                    }
                });
            }
            this.arText.forEach(function (item) {
                item.textEl.text(brease.language.getTextByKey(item.key));
            });
        };

        return languageDependency.decorate(WidgetClass, true);

    });
