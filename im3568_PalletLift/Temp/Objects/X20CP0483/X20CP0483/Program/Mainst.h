#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_MainStep_enum
#define __AS__TYPE_MainStep_enum
typedef enum MainStep_enum
{	WAIT = 0,
	GO_UP = 1,
	GO_DOWN = 2,
	ERROR = 3,
} MainStep_enum;
#endif

#ifndef __AS__TYPE_LiftStatus_typ
#define __AS__TYPE_LiftStatus_typ
typedef struct LiftStatus_typ
{	plcbit Error;
} LiftStatus_typ;
#endif

#ifndef __AS__TYPE_LiftCommands_typ
#define __AS__TYPE_LiftCommands_typ
typedef struct LiftCommands_typ
{	plcbit Up;
	plcbit Down;
	plcbit Reset;
} LiftCommands_typ;
#endif

#ifndef __AS__TYPE_LiftInterface_typ
#define __AS__TYPE_LiftInterface_typ
typedef struct LiftInterface_typ
{	LiftStatus_typ Status;
	LiftCommands_typ Cmds;
} LiftInterface_typ;
#endif

#ifndef __AS__TYPE_ActuatorInputs_typ
#define __AS__TYPE_ActuatorInputs_typ
typedef struct ActuatorInputs_typ
{	plcbit MinusWarn;
	plcbit MinusLimit;
	plcbit PlusWarn;
	plcbit PlusLimit;
	plcbit Clamped;
	plcbit Unclampled;
	plcbit SafetyStatus;
	plcbit Active;
	plcbit Fault;
} ActuatorInputs_typ;
#endif

#ifndef __AS__TYPE_DigitalInputs_typ
#define __AS__TYPE_DigitalInputs_typ
typedef struct DigitalInputs_typ
{	ActuatorInputs_typ RightActuator;
	ActuatorInputs_typ LeftActuator;
	plcbit PalletDetect;
} DigitalInputs_typ;
#endif

#ifndef __AS__TYPE_ActuatorOutputs_typ
#define __AS__TYPE_ActuatorOutputs_typ
typedef struct ActuatorOutputs_typ
{	plcbit DI1_StartStop;
	plcbit DI2_ForwardReverse;
	plcbit DI3_SpeedSelection;
	plcbit DI4_SpeedSelection;
	plcbit Unclamp;
	plcbit Clamp;
} ActuatorOutputs_typ;
#endif

#ifndef __AS__TYPE_StackLight_typ
#define __AS__TYPE_StackLight_typ
typedef struct StackLight_typ
{	plcbit RedLight;
	plcbit YellowLight;
	plcbit GreenLight;
} StackLight_typ;
#endif

#ifndef __AS__TYPE_DigitalOutputs_typ
#define __AS__TYPE_DigitalOutputs_typ
typedef struct DigitalOutputs_typ
{	ActuatorOutputs_typ RightActuator;
	ActuatorOutputs_typ LeftActuator;
	StackLight_typ Lights;
	plcbit Enable;
	plcbit FaultReset;
	plcbit Mute1;
	plcbit Mute2;
} DigitalOutputs_typ;
#endif

struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
_BUR_LOCAL MainStep_enum MainStep;
_BUR_LOCAL LiftInterface_typ Lift;
_BUR_LOCAL struct TON ExtraForceDelayTON;
_GLOBAL DigitalInputs_typ Inputs;
_GLOBAL DigitalOutputs_typ Outputs;
_LOCAL plcbit Edge0000100000;
_LOCAL plcbit Edge0000100001;
_LOCAL plcbit Edge0000100002;
_LOCAL plcbit Edge0000100003;
_LOCAL plcbit Edge0000100004;
_LOCAL plcbit Edge0000100005;
