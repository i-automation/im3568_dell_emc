#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Program/Mainst.h"
#line 1 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.nodebug"
#line 2 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{
}}
#line 3 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.nodebug"
#line 5 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{
switch(MainStep){
case 0:{
if(((Inputs.LeftActuator.MinusLimit^1)&(Inputs.RightActuator.MinusLimit^1)&Lift.Cmds.Down)){
(MainStep=2);
}else if(((Inputs.LeftActuator.PlusLimit^1)&(Inputs.RightActuator.PlusLimit^1)&Lift.Cmds.Up)){
(MainStep=1);
}

}break;case 1:{
if((Lift.Cmds.Up&~Edge0000100000&1?((Edge0000100000=Lift.Cmds.Up&1),1):((Edge0000100000=Lift.Cmds.Up&1),0))){
(Outputs.Enable=1);

(Outputs.LeftActuator.DI1_StartStop=1);
(Outputs.LeftActuator.DI2_ForwardReverse=0);
(Outputs.LeftActuator.DI3_SpeedSelection=1);
(Outputs.LeftActuator.DI4_SpeedSelection=0);

(Outputs.RightActuator.DI1_StartStop=1);
(Outputs.RightActuator.DI2_ForwardReverse=0);
(Outputs.RightActuator.DI3_SpeedSelection=1);
(Outputs.RightActuator.DI4_SpeedSelection=0);

(Outputs.Mute1=1);
(Outputs.Mute2=1);
}else if((Inputs.LeftActuator.PlusWarn&~Edge0000100001&1?((Edge0000100001=Inputs.LeftActuator.PlusWarn&1),1):((Edge0000100001=Inputs.LeftActuator.PlusWarn&1),0))){
(Outputs.Enable=1);

(Outputs.LeftActuator.DI1_StartStop=1);
(Outputs.LeftActuator.DI2_ForwardReverse=0);
(Outputs.LeftActuator.DI3_SpeedSelection=0);
(Outputs.LeftActuator.DI4_SpeedSelection=1);

(Outputs.RightActuator.DI1_StartStop=1);
(Outputs.RightActuator.DI2_ForwardReverse=0);
(Outputs.RightActuator.DI3_SpeedSelection=0);
(Outputs.RightActuator.DI4_SpeedSelection=1);

(Outputs.Mute1=1);
(Outputs.Mute2=1);
}else if((Inputs.LeftActuator.PlusLimit&~Edge0000100002&1?((Edge0000100002=Inputs.LeftActuator.PlusLimit&1),1):((Edge0000100002=Inputs.LeftActuator.PlusLimit&1),0))){
(Lift.Cmds.Up=0);
(Outputs.Enable=0);

(Outputs.LeftActuator.DI1_StartStop=0);
(Outputs.LeftActuator.DI2_ForwardReverse=0);
(Outputs.LeftActuator.DI3_SpeedSelection=0);
(Outputs.LeftActuator.DI4_SpeedSelection=0);

(Outputs.RightActuator.DI1_StartStop=0);
(Outputs.RightActuator.DI2_ForwardReverse=0);
(Outputs.RightActuator.DI3_SpeedSelection=0);
(Outputs.RightActuator.DI4_SpeedSelection=0);

(Outputs.Mute1=0);
(Outputs.Mute2=0);
(MainStep=0);
}

}break;case 2:{
if((Lift.Cmds.Down&~Edge0000100003&1?((Edge0000100003=Lift.Cmds.Down&1),1):((Edge0000100003=Lift.Cmds.Down&1),0))){
(Outputs.Enable=1);

(Outputs.LeftActuator.DI1_StartStop=1);
(Outputs.LeftActuator.DI2_ForwardReverse=1);
(Outputs.LeftActuator.DI3_SpeedSelection=1);
(Outputs.LeftActuator.DI4_SpeedSelection=0);

(Outputs.RightActuator.DI1_StartStop=1);
(Outputs.RightActuator.DI2_ForwardReverse=1);
(Outputs.RightActuator.DI3_SpeedSelection=1);
(Outputs.RightActuator.DI4_SpeedSelection=0);

(Outputs.Mute1=1);
(Outputs.Mute2=1);
}else if((Inputs.LeftActuator.MinusWarn&~Edge0000100004&1?((Edge0000100004=Inputs.LeftActuator.MinusWarn&1),1):((Edge0000100004=Inputs.LeftActuator.MinusWarn&1),0))){
(Outputs.Enable=1);

(Outputs.LeftActuator.DI1_StartStop=1);
(Outputs.LeftActuator.DI2_ForwardReverse=1);
(Outputs.LeftActuator.DI3_SpeedSelection=0);
(Outputs.LeftActuator.DI4_SpeedSelection=1);

(Outputs.RightActuator.DI1_StartStop=1);
(Outputs.RightActuator.DI2_ForwardReverse=1);
(Outputs.RightActuator.DI3_SpeedSelection=0);
(Outputs.RightActuator.DI4_SpeedSelection=1);

(Outputs.Mute1=1);
(Outputs.Mute2=1);
}else if((Inputs.LeftActuator.MinusLimit&~Edge0000100005&1?((Edge0000100005=Inputs.LeftActuator.MinusLimit&1),1):((Edge0000100005=Inputs.LeftActuator.MinusLimit&1),0))){

(ExtraForceDelayTON.IN=1);
(ExtraForceDelayTON.PT=250);
}
if(ExtraForceDelayTON.Q){
(ExtraForceDelayTON.IN=0);

(Lift.Cmds.Down=0);
(Outputs.Enable=0);

(Outputs.LeftActuator.DI1_StartStop=0);
(Outputs.LeftActuator.DI2_ForwardReverse=0);
(Outputs.LeftActuator.DI3_SpeedSelection=0);
(Outputs.LeftActuator.DI4_SpeedSelection=0);

(Outputs.RightActuator.DI1_StartStop=0);
(Outputs.RightActuator.DI2_ForwardReverse=0);
(Outputs.RightActuator.DI3_SpeedSelection=0);
(Outputs.RightActuator.DI4_SpeedSelection=0);
(Outputs.Mute1=0);
(Outputs.Mute2=0);
(MainStep=0);
}
}break;case 3:{
if(Lift.Cmds.Reset){
(Lift.Cmds.Reset=0);
(Outputs.FaultReset=1);
(MainStep=0);
}

}break;}
if(((Inputs.LeftActuator.SafetyStatus^1)|(Inputs.RightActuator.SafetyStatus^1)|Inputs.LeftActuator.Fault|Inputs.RightActuator.Fault)){
(MainStep=3);
}
TON(&ExtraForceDelayTON);
}}
#line 131 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.nodebug"
#line 133 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.st"
void _EXIT __BUR__ENTRY_EXIT_FUNCT__(void){{
}}
#line 134 "C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Program/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/repos/DellEMC/im3568_PalletLift/Temp/Objects/X20CP0483/X20CP0483/Program/Main.st.c\\\" \\\"C:/repos/DellEMC/im3568_PalletLift/Logical/Program/Main.st\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Objects/X20CP0483/X20CP0483/Program/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".previous");
