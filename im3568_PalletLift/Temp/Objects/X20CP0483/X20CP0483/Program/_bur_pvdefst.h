#ifndef __AS__TYPE_MainStep_enum
#define __AS__TYPE_MainStep_enum
typedef enum MainStep_enum
{	WAIT = 0,
	GO_UP = 1,
	GO_DOWN = 2,
	ERROR = 3,
} MainStep_enum;
#endif

#ifndef __AS__TYPE_LiftStatus_typ
#define __AS__TYPE_LiftStatus_typ
typedef struct LiftStatus_typ
{	plcbit Error;
} LiftStatus_typ;
#endif

#ifndef __AS__TYPE_LiftCommands_typ
#define __AS__TYPE_LiftCommands_typ
typedef struct LiftCommands_typ
{	plcbit Up;
	plcbit Down;
	plcbit Reset;
} LiftCommands_typ;
#endif

#ifndef __AS__TYPE_LiftInterface_typ
#define __AS__TYPE_LiftInterface_typ
typedef struct LiftInterface_typ
{	LiftStatus_typ Status;
	LiftCommands_typ Cmds;
} LiftInterface_typ;
#endif

struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
_BUR_LOCAL MainStep_enum MainStep;
_BUR_LOCAL LiftInterface_typ Lift;
_BUR_LOCAL struct TON ExtraForceDelayTON;
