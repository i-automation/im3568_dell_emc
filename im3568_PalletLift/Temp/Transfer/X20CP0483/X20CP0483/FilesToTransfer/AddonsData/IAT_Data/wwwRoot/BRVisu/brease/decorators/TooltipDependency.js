define(['brease/core/Decorator', 'brease/core/Utils', 'brease/events/BreaseEvent', 'brease/enum/Enum', 'brease/controller/PopUpManager', 'brease/controller/ZoomManager'], function (Decorator, Utils, BreaseEvent, Enum, PopUpManager, ZoomManager) {

    'use strict';

    var TooltipDependency = function TooltipDependency() {
            this.initType = Decorator.TYPE_PRE;
        },
        dependency = 'tooltip',
        event = 'language',
        changeHandler = 'tooltipChangeHandler',
        globalActivated = false;

    /**
    * @class brease.decorators.TooltipDependency
    * @extends brease.core.Decorator
    * #Description
    * A decorator class to add functionality of tooltip dependency to widgets.
    * ##Example:
    *
    *     define(function (require) {
    *        var SuperClass = require('brease/core/BaseWidget'),
    *            TooltipDependency = require('brease/decorators/TooltipDependency'),
    *            [...]
    *
    *        return TooltipDependency.decorate(WidgetClass);
    *     });
    *
    *
    * @iatMeta studio:visible
    * false
    */

    /**
    * @method decorate
    * decorate a widget class with functionality of user dependency
    * @param {brease.core.WidgetClass} widgetClass
    * @param {Boolean} initialDependency Initial dependency of widget instances
    * @return {brease.core.WidgetClass} returns decorated WidgetClass
    */
    TooltipDependency.prototype = new Decorator();
    TooltipDependency.prototype.constructor = TooltipDependency;

    var instance = new TooltipDependency();

    /**
    * @property {Object} methodsToAdd
    * @property {Function} methodsToAdd.setTooltipDependency
    * @property {Boolean} methodsToAdd.setTooltipDependency.flag
    * Enable or disable tooltip dependency; dependent widgets listen to tooltip changes and execute method *tooltipChangeHandler* on changes
    */
    instance.methodsToAdd = {

        init: function (initialDependency) {
            this.dependencies[dependency] = {
                state: Enum.Dependency.INACTIVE,
                stored: {
                    tooltipModeActive: false,
                    arrowPosition: 'n'
                },
                suspend: suspend.bind(this),
                wake: wake.bind(this),
                event: event
            };
            if (initialDependency === true) {
                this.setTooltipDependency(initialDependency);
            }
            this.tooltipIndicatorAttached = false;
            this.tooltipContentAttached = false;

            if (this.dependencies[dependency].stored.tooltipModeActive) {
                this.activateTooltipMode();
            }
        },
        tooltipChangeHandler: function () {
            if (_hasTooltip.call(this) && this.tooltipContentAttached) {
                _setTooltipContent.apply(this, [_parseTooltip(this.settings.tooltip)]);
                _positionTooltipContent.call(this);
            }
        },
        setTooltip: function (tooltip) {
            this.settings.tooltip = tooltip;
        },
        getTooltip: function () {
            return this.settings.tooltip;
        },
        showTooltip: function () {
            this.activateTooltipMode();
        },
        activateTooltipMode: function (local) {
            if (_hasTooltip.call(this) && this.isHidden === false) {
                _appendTooltipToDOM.call(this);
                _positionTooltipIndicator.call(this);

                if (this.dependencies[dependency]) {
                    if (!this.dependencies[dependency].stored.tooltipModeActive) {
                        this.dependencies[dependency].stored.tooltipModeActive = true;
                        if (globalActivated === false && local !== true) {
                            globalActivated = true;
                            document.body.dispatchEvent(new CustomEvent(BreaseEvent.TOOLTIPMODE_ACTIVE));
                        }
                    }
                }
            }
        },
        // called when exiting from tooltip mode
        deactivateTooltipMode: function (local) {
            _detachTooltipIndicator.call(this);
            _detachTooltipContent.call(this);
            if (this.dependencies[dependency]) {
                if (this.dependencies[dependency].stored.tooltipModeActive) {
                    this.dependencies[dependency].stored.tooltipModeActive = false;
                    if (globalActivated === true && local !== true) {
                        globalActivated = false;
                        document.body.dispatchEvent(new CustomEvent(BreaseEvent.TOOLTIPMODE_INACTIVE));
                    }
                }
            }
        },

        setTooltipDependency: function (flag) {
            if (flag === true) {
                setState.call(this, Enum.Dependency.ACTIVE);
            } else {
                setState.call(this, Enum.Dependency.INACTIVE);
            }
        },

        _visibleHandler: function () {
            if (this.isHidden && this.dependencies[dependency].stored.tooltipModeActive) {
                this.deactivateTooltipMode(true);
            }
            if (!this.isHidden && !this.dependencies[dependency].stored.tooltipModeActive && globalActivated) {
                this.activateTooltipMode(true);
            }
        },

        dispose: function () {
            this.dependencies[dependency] = null;
            removeListener.call(this);
            brease.bodyEl.off(BreaseEvent.MOUSE_DOWN, this._bind(_onHMIOperation));
            this.deactivateTooltipMode();
            this.tooltipIndicatorAttached = null;
            this.tooltipContentAttached = null;

            this.tooltipElements = null;
            this.tooltipIndicator = null;
            this.tooltipIndicatorInner = null;
            this.tooltipContentOutterWrapper = null;
            this.tooltipContent = null;
            this.tooltipContentText = null;
            this.tooltipContentArrow = null;
        }

    };

    function suspend() {
        if (this.dependencies[dependency].state === Enum.Dependency.ACTIVE) {

            this.dependencies[dependency].stored.code = brease.language.getCurrentLanguage();
            this.dependencies[dependency].stored.version = brease.language.getCurrentVersion();

            setState.call(this, Enum.Dependency.SUSPENDED);
        }
    }

    function wake(e) {
        if (this.dependencies[dependency].state === Enum.Dependency.SUSPENDED) {
            setState.call(this, Enum.Dependency.ACTIVE);
            if (this.dependencies[dependency].stored.code !== brease.language.getCurrentLanguage() ||
                this.dependencies[dependency].stored.version !== brease.language.getCurrentVersion()) {
                this[changeHandler](e);
            }
        }
    }

    function setState(state) {
        //console.log('%c' + this.elem.id + '.dependencies[' + dependency + '].state=' + state, 'color:#cccc00');
        this.dependencies[dependency].state = state;
        if (state === Enum.Dependency.ACTIVE) {
            addListener.call(this);
        } else {
            removeListener.call(this);
            this.deactivateTooltipMode();
        }
    }

    // add listeners to framework events which have an impact on the tooltip (e.g.: language change)
    function addListener() {
        document.body.addEventListener(BreaseEvent.LANGUAGE_CHANGED, this._bind(changeHandler));
    }

    function removeListener() {
        document.body.removeEventListener(BreaseEvent.LANGUAGE_CHANGED, this._bind(changeHandler));
    }

    // returns wether the widget has a valid tooltip configured and is allowed to display the tooltip. (visibility is also taken into account)
    function _hasTooltip() {
        return (this.settings.tooltip.length > 0) &&
            (this.settings.parentContentId &&
                this.settings.parentContentId !== brease.settings.globalContent);
    }

    // creates the element for the tooltip indicator
    function _createTooltipIndicator() {
        //this.tooltipIndicatorClone = $('<div class="breaseTooltipIndicator system_brease_Tooltip_style_default" style="box-sizing:border-box;width:auto; height:auto;position:absolute; visibility:hidden;" ></div>');
        this.tooltipIndicator = $('<div class="breaseTooltipIndicator system_brease_Tooltip_style_default" data-source="' + this.elem.id + '" style="box-sizing:border-box;width:auto; height:auto; position:absolute !important; z-index:1;"></div>');
        this.tooltipIndicatorInner = $('<div class="breaseTooltipIndicatorInner"></div>');
        this.tooltipIndicator.append(this.tooltipIndicatorInner);
    }

    // creates the container element for the tooltip content
    function _createTooltipContent() {
        this.tooltipContentOutterWrapper = $('<div class="breaseTooltipOutterWrapper system_brease_Tooltip_style_default" data-source="' + this.elem.id + '" style="max-width:' + brease.appView.get(0).offsetWidth / ZoomManager.getAppZoom() + 'px;"></div>');
        this.tooltipContent = $('<div class="breaseTooltip n"></div>');
        this.tooltipContentText = $('<div class="textDiv breaseTooltipText"></div>');

        this.tooltipContentArrow = $('<div class="tooltip-arrow"></div>');
        this.tooltipContentArrowBorder = $('<div class="tooltip-arrow-border"></div>');
        this.tooltipContentArrowBackground = $('<div class="tooltip-arrow-background"></div>');

        this.tooltipContentArrow.append(this.tooltipContentArrowBorder);
        this.tooltipContentArrow.append(this.tooltipContentArrowBackground);

        this.tooltipContentOutterWrapper.append(this.tooltipContent);
        this.tooltipContentOutterWrapper.append(this.tooltipContentArrow);
        this.tooltipContent.append(this.tooltipContentText);

        this.tooltipElements = {
            tooltipIndicator: this.tooltipIndicator,
            tooltipIndicatorInner: this.tooltipIndicatorInner,
            tooltipContentOutterWrapper: this.tooltipContentOutterWrapper,
            tooltipContent: this.tooltipContent,
            tooltipContentText: this.tooltipContentText,
            tooltipContentArrow: this.tooltipContentArrow
        };
    }

    //******************************//
    //*** TOOLTIP EVENT HANDLING ***//
    //******************************//

    // add listeners to the tooltip indicator
    function _addTooltipListeners() {
        var widget = this;
        brease.bodyEl.on(BreaseEvent.MOUSE_DOWN, this._bind(_onHMIOperation));
        this.tooltipIndicator.on(BreaseEvent.CLICK, this._bind(_appendTooltipContent));
        // this.tooltipIndicator.on('mouseenter',this._bind(_appendTooltipContent));
        // this.tooltipIndicator.on('mouseleave',this._bind(_appendTooltipContent));
        this.tooltipIndicator.on(BreaseEvent.MOUSE_DOWN, function (e) {
            widget._handleEvent(e, true);
        });

        $(window).resize(function () {
            widget.deactivateTooltipMode();
        });
        $(window).on('mousewheel', function (e) {
            widget.deactivateTooltipMode();
        });
    }

    function _targetIsInTooltip(targetEl) {
        return targetEl.closest('.breaseTooltipOutterWrapper').length > 0;
    }

    function _targetIsTooltipIndicator(targetEl) {
        return targetEl.hasClass('tooltipindicator');
    }

    // executed when the HMI is operated while tooltip mode is active
    function _onHMIOperation(e) {
        var targetEl = $(e.target);
        if (!_targetIsTooltipIndicator(targetEl) && !_targetIsInTooltip(targetEl)) {
            brease.bodyEl.off(BreaseEvent.MOUSE_DOWN, this._bind(_onHMIOperation));
            this.deactivateTooltipMode();
        }
    }

    // remove listeners from the tooltip indicator
    function _removeTooltipListeners() {
        brease.bodyEl.off(BreaseEvent.MOUSE_DOWN, this._bind(_onHMIOperation));
        this.tooltipIndicator.off();
    }

    //*************************//
    //*** TOOLTIP INDICATOR ***//
    //*************************//

    // append tooltip indicator and tooltipContent to the DOM
    function _appendTooltipToDOM() {
        if (!this.tooltipIndicatorAttached) {
            _createTooltipIndicator.call(this);
            _addTooltipListeners.call(this);
            this.tooltipIndicatorAttached = true;
        }

        if (!this.tooltipContentAttached) {
            _createTooltipContent.call(this);
            _setTooltipContent.apply(this, [_parseTooltip(this.settings.tooltip)]);
        }
    }

    // tooltip indicator is positioned to the top right corner of the target element (widget)
    // the indicator is appended to the parent element of the widget in order to be hidden
    // when the widget is inside of a scrollable container
    function _positionTooltipIndicator() {
        var cssTooltipIndicatorClone, css, transform, tooltipIndicatorClientRect, tooltipIndicatorInnerClientRect,
            rotation = Utils.getMatrix(this.elem),
            zoomFactor = _getZoomWithTooltipIndicator(this.el, this.elem, rotation),
            parentPositionedContainer = Utils.getPositionedParent(this.elem),
            widgetElemClientRect = this.elem.getBoundingClientRect(),
            parentContainer = parentPositionedContainer.getBoundingClientRect(),
            widgetRectLeft = widgetElemClientRect.left - parentContainer.left,
            widgetRectTop = widgetElemClientRect.top - parentContainer.top,
            widgetRect = {
                w: this.el.outerWidth() || widgetElemClientRect.width / zoomFactor,
                h: this.el.outerHeight() || widgetElemClientRect.height / zoomFactor,
                left: widgetRectLeft / zoomFactor,
                top: widgetRectTop / zoomFactor
            };

        this.el.parent().append(this.tooltipIndicator);
        tooltipIndicatorClientRect = this.tooltipIndicator.get(0).getBoundingClientRect();
        tooltipIndicatorInnerClientRect = this.tooltipIndicatorInner.get(0).getBoundingClientRect();
        var outerTouchAreaTooltipRight = (tooltipIndicatorClientRect.width - tooltipIndicatorInnerClientRect.width) / 2 / zoomFactor,
            outerTouchAreaTooltipTop = (tooltipIndicatorClientRect.height - tooltipIndicatorInnerClientRect.height) / 2 / zoomFactor;
        transform = 'matrix(';
        for (var i = 0; i < rotation.length; i = i + 1) {
            i === rotation.length - 1 ? transform = transform + rotation[i] : transform = transform + rotation[i] + ',';
        }
        transform = transform + ')';

        var offset = { top: 0, left: 0 };
        css = {
            transform: transform,
            'z-index': this.el.css('z-index') + 1,
            top: 0,
            left: 0
        };
        this.tooltipIndicator.css(css);
        tooltipIndicatorClientRect = this.tooltipIndicator.get(0).getBoundingClientRect();
        tooltipIndicatorInnerClientRect = this.tooltipIndicatorInner.get(0).getBoundingClientRect();

        if (rotation[0] !== '1' && rotation[0] !== 'none' && rotation[0] !== undefined) {
            this.tooltipIndicatorClone = $('<div class="breaseTooltipIndicator system_brease_Tooltip_style_default" style="box-sizing:border-box;width:auto; height:auto;position:absolute; visibility:hidden;" ></div>');
            this.el.append(this.tooltipIndicatorClone);
            cssTooltipIndicatorClone = {
                right: '-' + outerTouchAreaTooltipRight + 'px',
                top: '-' + outerTouchAreaTooltipTop + 'px'
            };
            this.tooltipIndicatorClone.css(cssTooltipIndicatorClone);
            var IndicatorCloneDomRect = this.tooltipIndicatorClone.get(0).getBoundingClientRect();

            offset.left = (IndicatorCloneDomRect.left - tooltipIndicatorClientRect.left) / zoomFactor;
            offset.top = (IndicatorCloneDomRect.top - tooltipIndicatorClientRect.top) / zoomFactor;
        } else {
            offset.top = widgetRect.top - this.tooltipIndicatorInner.get(0).offsetTop;
            offset.left = widgetRect.left + widgetRect.w - this.tooltipIndicator.outerWidth() + this.tooltipIndicatorInner.get(0).offsetLeft;
        }
        this.tooltipIndicator.css(offset);
    }

    // resolves the rotate transformation and returns
    // sinAlpha and cosAlpha to position the tooltipIndicator on rotated
    // widgets
    //function _getElementRotation(el) {
    //    var transform,
    //        transformMatrix = [],
    //        rotation = { sinAlpha: 0, colAlpha: 1, transform: '' };

    //    return rotation;
    //}

    // remove tooltip indicator from the DOM
    function _detachTooltipIndicator() {
        if (this.tooltipIndicatorAttached) {
            _removeTooltipListeners.call(this);
            this.tooltipIndicator.detach();
            this.tooltipIndicatorAttached = false;
        }
    }

    //***********************//
    //*** TOOLTIP CONTENT ***//
    //***********************//

    // append tooltip content to the DOM
    // if the content is already appended it toggles between visible/hidden
    function _appendTooltipContent(e) {
        if (!_hasTooltip.call(this) || this.isHidden) {
            return;
        }
        if (e) {
            this._handleEvent(e, true);
        }
        if (!this.tooltipContentAttached) {
            this.tooltipContentAttached = true;
        } else {
            this.tooltipContentOutterWrapper.css('visibility') === 'hidden' ? this.tooltipContentOutterWrapper.css('visibility', 'visible') : this.tooltipContentOutterWrapper.css('visibility', 'hidden');
        }
        if (this.tooltipContentOutterWrapper.css('visibility') !== 'hidden') {
            _positionTooltipContent.call(this);
        }
    }

    // returns the data for the tooltip. resolves the textkey in case of a localizable text
    function _parseTooltip(tooltip) {
        if (brease.language.isKey(tooltip)) {
            return brease.language.getTextByKey(brease.language.parseKey(tooltip));
        } else {
            return tooltip;
        }
    }

    // set the content for the tooltip
    function _setTooltipContent(content) {
        //content = content.replace(/(\r\n|\n|\r)/gm, '<br/>');
        if (content.match(/(\r\n|\n|\r)/gm) !== null) {
            this.tooltipElements.tooltipContentText.css('white-space', 'pre');
        }

        this.tooltipContentText.text(content);
    }

    // boundaries of the appContainer are taken into account because the content is appended to
    // the document body
    //function _positionTooltipContent(tooltipElements, target, position, inner) {
    function _positionTooltipContent() {
        var zoom = ZoomManager.getAppZoom();
        this.tooltipContentOutterWrapper.css({ 'transform': 'scale(' + zoom + ',' + zoom + ')', 'transform-origin': '0 0' });
        brease.bodyEl.append(this.tooltipContentOutterWrapper);
        var offsetFlag = false;
        if (this.tooltipContentOutterWrapper.offset().left === 0 && this.tooltipContentOutterWrapper.offset().top === 0) {
            offsetFlag = true;
        }
        var tooltipIndicatorInnerOffset = this.tooltipIndicatorInner.offset(),
            tooltipIndicatorInnerRect = this.tooltipIndicatorInner.get(0).getBoundingClientRect(),
            tooltipContentArrowRect = this.tooltipContentArrow.get(0).getBoundingClientRect(),
            tooltipContentRect = this.tooltipContent.get(0).getBoundingClientRect(),
            middleIndicatorPosition = tooltipIndicatorInnerOffset.left + (tooltipIndicatorInnerRect.width / 2),
            contentPlusArrowHeight = tooltipContentRect.height + tooltipContentArrowRect.height,
            offset = { top: 0, left: 0 },

            // limit border radius and
            // consider corner radius and move arrow away from the rounded edges -> if the tooltip overlaps left boundaries
            borderRadius = parseFloat(this.tooltipContent.css('border-radius'));
        borderRadius = Math.min(borderRadius, 20);

        var cornerRadiusBalancing = borderRadius / 2,
            newStyle = {
                'borderRadius': borderRadius + 'px'
            };
        this.tooltipContent.css(newStyle);
        offset.left = middleIndicatorPosition - tooltipContentRect.width / 2;
        if (middleIndicatorPosition - (tooltipContentRect.width / 2) <= 0) {
            offset.left = 0 - cornerRadiusBalancing / 2;
        } else {
            // overlap right boundaries
            if (middleIndicatorPosition + (tooltipContentRect.width / 2) >= brease.appView.get(0).getBoundingClientRect().width) {
                offset.left = brease.appView.get(0).getBoundingClientRect().width - tooltipContentRect.width;
            }
        }
        // overlap top boundaries
        offset.top = tooltipIndicatorInnerRect.top - contentPlusArrowHeight;

        if (tooltipIndicatorInnerRect.top - contentPlusArrowHeight <= 0) {
            this.tooltipContentArrow.addClass('n');
            offset.top = tooltipIndicatorInnerRect.top + tooltipIndicatorInnerRect.height + tooltipContentArrowRect.height;
        }

        var css = {
            'min-width': (tooltipContentRect.width) / zoom + 'px',
            'max-width': brease.appView.get(0).offsetWidth / zoom + 'px',
            'z-index': PopUpManager.getHighestZindex() + 1,
            'top': offset.top + 'px',
            'left': offset.left + 'px'
        };

        this.tooltipContentOutterWrapper.css(css);
        var arrowOffset = {
            'top': 0,
            'left': 0
        };
        contentPlusArrowHeight = this.tooltipContent.get(0).getBoundingClientRect().height + tooltipContentArrowRect.height;
        if (offsetFlag) {
            arrowOffset.top = tooltipIndicatorInnerRect.top - tooltipContentArrowRect.height;
            if (tooltipIndicatorInnerRect.top - contentPlusArrowHeight <= 0) {
                this.tooltipContentArrow.addClass('n');
                arrowOffset.top = tooltipIndicatorInnerRect.top + tooltipIndicatorInnerRect.height / 2 + tooltipContentArrowRect.height;
            }
            arrowOffset.left = this.tooltipContentArrow.offset().left + (middleIndicatorPosition - this.tooltipContentOutterWrapper.offset().left) / zoom;

            this.tooltipContentArrow.offset(arrowOffset);
        }
    }

    // detach tooltip content from the dom
    function _detachTooltipContent() {
        if (this.tooltipContentAttached) {
            this.tooltipContentOutterWrapper.detach();
            this.tooltipContentAttached = false;
        }
    }

    function _getZoomWithTooltipIndicator($elem, elem, matrix) {
        var factor = 1,
            width = $elem.outerWidth();
        if (matrix[1] === '1' || matrix[1] === '-1') {
            if (width > 0) {
                factor = elem.getBoundingClientRect().height / width;
            }
        } else if (width > 0) {
            var transform = 'matrix(';
            for (var i = 0; i < matrix.length; i = i + 1) {
                i === matrix.length - 1 ? transform = transform + matrix[i] : transform = transform + matrix[i] + ',';
            }
            transform = transform + ')';
            $elem.css('transform', 'matrix(1,0,0,1,0,0)');
            factor = elem.getBoundingClientRect().width / width;
            $elem.css('transform', transform);
        }
        return factor;
    }

    return instance;
});
