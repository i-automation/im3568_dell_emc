define(['brease/events/BreaseEvent'], function (BreaseEvent) {

    'use strict';

    var BindingControllerStub = function BindingControllerStub(timeout) {
        this.contents = {};
        this.activateFail = [];
        this.deactivateFail = [];
        this.timeout = timeout || 0;
    };

    var p = BindingControllerStub.prototype;

    p.reset = function () {
        this.contents = {};
        this.activateFail = [];
        this.deactivateFail = [];
    };

    p.activateContent = function (contentId) {
        console.warn('activateContent:' + contentId);
        var deferred = $.Deferred();
        if (this.timeout > 0) {
            window.setTimeout(activateContent.bind(this, contentId, deferred), this.timeout); 
        } else {
            activateContent.call(this, contentId, deferred); 
        }
        return deferred.promise();
    };

    p.deactivateContent = function (contentId) {
        console.warn('deactivateContent:' + contentId);
        var deferred = $.Deferred();
        if (this.timeout > 0) {
            window.setTimeout(deactivateContent.bind(this, contentId, deferred), this.timeout); 
        } else {
            deactivateContent.call(this, contentId, deferred); 
        }
        return deferred.promise();
    };

    p.sendInitialValues = function (contentId, callback) { if (typeof callback === 'function') { callback(contentId); } };

    p.getSubscriptionsForElement = function () { return []; };

    p.allActive = function () {
        return true;
    };

    p.setContentState = function (contentId, state) {
        this.contents[contentId] = state;
    };

    p.isContentActive = function (contentId) {
        console.warn('isContentActive:' + contentId + ':' + (this.contents[contentId] === true));
        return (this.contents[contentId] === true);
    };

    p.attributeChangeForwarder = function () { console.info('attributeChangeForwarder'); };

    function activateContent(contentId, deferred) {
        if (this.activateFail.indexOf(contentId) === -1) {
            console.warn('%c' + 'activateContent:' + contentId, 'color:green');
            this.contents[contentId] = true;
            document.body.dispatchEvent(new CustomEvent(BreaseEvent.CONTENT_ACTIVATED, { detail: { contentId: contentId } }));
            deferred.resolve(contentId); 
        } else {
            console.log('%c' + 'activate of content ' + contentId + ' failed', 'color:red;');
        }
    }
    function deactivateContent(contentId, deferred) {
        if (this.deactivateFail.indexOf(contentId) === -1) {
            console.log('%c' + 'deactivateContent:' + contentId, 'color:red');
            this.contents[contentId] = false;
            document.body.dispatchEvent(new CustomEvent(BreaseEvent.CONTENT_DEACTIVATED, { detail: { contentId: contentId } }));
            deferred.resolve(contentId);
        } else {
            console.log('%c' + 'deactivate of content ' + contentId + ' failed', 'color:red;');
        }
    }

    return BindingControllerStub;

});
