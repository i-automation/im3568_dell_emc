
TYPE
	MainStep_enum : 
		(
		WAIT,
		GO_UP,
		GO_DOWN,
		ERROR
		);
	LiftInterface_typ : 	STRUCT 
		Status : LiftStatus_typ;
		Cmds : LiftCommands_typ;
	END_STRUCT;
	LiftStatus_typ : 	STRUCT 
		Error : BOOL;
	END_STRUCT;
	LiftCommands_typ : 	STRUCT 
		Up : BOOL;
		Down : BOOL;
		Reset : BOOL;
	END_STRUCT;
END_TYPE
