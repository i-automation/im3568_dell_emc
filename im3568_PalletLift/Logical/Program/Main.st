
PROGRAM _INIT
END_PROGRAM

PROGRAM _CYCLIC
	CASE MainStep OF
		WAIT://wait for HMI button commands
			IF NOT Inputs.LeftActuator.MinusLimit AND NOT Inputs.RightActuator.MinusLimit AND  Lift.Cmds.Down THEN
				MainStep := GO_DOWN;
			ELSIF NOT Inputs.LeftActuator.PlusLimit AND NOT Inputs.RightActuator.PlusLimit AND  Lift.Cmds.Up THEN
				MainStep := GO_UP;
			END_IF;
			
		GO_UP://go up
			IF EDGEPOS(Lift.Cmds.Up) THEN
				Outputs.Enable := TRUE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= TRUE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= TRUE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= FALSE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= TRUE;
				Outputs.RightActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.RightActuator.DI3_SpeedSelection	:= TRUE;
				Outputs.RightActuator.DI4_SpeedSelection	:= FALSE;
				//mute Light Curtain
				Outputs.Mute1 := TRUE;
				Outputs.Mute2 := TRUE;				
			ELSIF EDGEPOS(Inputs.LeftActuator.PlusWarn) THEN
				Outputs.Enable := TRUE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= TRUE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= TRUE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= TRUE;
				Outputs.RightActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.RightActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.RightActuator.DI4_SpeedSelection	:= TRUE;
				//mute Light Curtain
				Outputs.Mute1 := TRUE;
				Outputs.Mute2 := TRUE;
			ELSIF EDGEPOS(Inputs.LeftActuator.PlusLimit) THEN
				Lift.Cmds.Up := FALSE;
				Outputs.Enable := FALSE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= FALSE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= FALSE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= FALSE;
				Outputs.RightActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.RightActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.RightActuator.DI4_SpeedSelection	:= FALSE;
				//unmute Light Curtain
				Outputs.Mute1 := FALSE;
				Outputs.Mute2 := FALSE;		
				MainStep := WAIT;
			END_IF;

		GO_DOWN://go down
			IF EDGEPOS(Lift.Cmds.Down) THEN
				Outputs.Enable := TRUE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= TRUE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= TRUE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= TRUE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= FALSE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= TRUE;
				Outputs.RightActuator.DI2_ForwardReverse	:= TRUE;
				Outputs.RightActuator.DI3_SpeedSelection	:= TRUE;
				Outputs.RightActuator.DI4_SpeedSelection	:= FALSE;
				//mute Light Curtain
				Outputs.Mute1 := TRUE;
				Outputs.Mute2 := TRUE;				
			ELSIF EDGEPOS(Inputs.LeftActuator.MinusWarn) THEN
				Outputs.Enable := TRUE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= TRUE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= TRUE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= TRUE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= TRUE;
				Outputs.RightActuator.DI2_ForwardReverse	:= TRUE;
				Outputs.RightActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.RightActuator.DI4_SpeedSelection	:= TRUE;
				//mute Light Curtain
				Outputs.Mute1 := TRUE;
				Outputs.Mute2 := TRUE;
			ELSIF EDGEPOS(Inputs.LeftActuator.MinusLimit) THEN
				//put a timer here
				ExtraForceDelayTON.IN := TRUE;
				ExtraForceDelayTON.PT := T#0s250ms;				
			END_IF;	
			IF ExtraForceDelayTON.Q THEN		//This delay introduced per Brian request to exercise extra force once Lift at the bottom only!
				ExtraForceDelayTON.IN := FALSE;
					
				Lift.Cmds.Down := FALSE;
				Outputs.Enable := FALSE;
				//first VFD
				Outputs.LeftActuator.DI1_StartStop	:= FALSE;
				Outputs.LeftActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.LeftActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.LeftActuator.DI4_SpeedSelection	:= FALSE;
				//second VFD
				Outputs.RightActuator.DI1_StartStop	:= FALSE;
				Outputs.RightActuator.DI2_ForwardReverse	:= FALSE;
				Outputs.RightActuator.DI3_SpeedSelection	:= FALSE;
				Outputs.RightActuator.DI4_SpeedSelection	:= FALSE;
				Outputs.Mute1 := FALSE;
				Outputs.Mute2 := FALSE;		
				MainStep := WAIT;
			END_IF;
		ERROR://wait for reset command from HMI to reset and go back to wait state
			IF Lift.Cmds.Reset THEN
				Lift.Cmds.Reset := FALSE;
				Outputs.FaultReset := TRUE;
				MainStep := WAIT;
			END_IF;
			
	END_CASE;
	IF (NOT Inputs.LeftActuator.SafetyStatus) OR (NOT Inputs.RightActuator.SafetyStatus) OR Inputs.LeftActuator.Fault OR Inputs.RightActuator.Fault THEN
		MainStep := ERROR;
	END_IF;
	ExtraForceDelayTON();
END_PROGRAM

PROGRAM _EXIT	 
END_PROGRAM

