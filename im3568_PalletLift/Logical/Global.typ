
TYPE
	DigitalInputs_typ : 	STRUCT 
		RightActuator : ActuatorInputs_typ;
		LeftActuator : ActuatorInputs_typ;
		PalletDetect : BOOL;
	END_STRUCT;
	DigitalOutputs_typ : 	STRUCT 
		RightActuator : ActuatorOutputs_typ;
		LeftActuator : ActuatorOutputs_typ;
		Lights : StackLight_typ;
		Enable : BOOL;
		FaultReset : BOOL;
		Mute1 : BOOL;
		Mute2 : BOOL;
	END_STRUCT;
	ActuatorInputs_typ : 	STRUCT 
		MinusWarn : BOOL;
		MinusLimit : BOOL;
		PlusWarn : BOOL;
		PlusLimit : BOOL;
		Clamped : BOOL;
		Unclampled : BOOL;
		SafetyStatus : BOOL;
		Active : BOOL;
		Fault : BOOL;
	END_STRUCT;
	ActuatorOutputs_typ : 	STRUCT 
		DI1_StartStop : BOOL; (*Stop (0) . Start (1)*)
		DI2_ForwardReverse : BOOL; (*Forward (0) . Reverse 1)*)
		DI3_SpeedSelection : BOOL; (*speed selection*)
		DI4_SpeedSelection : BOOL; (*speed selection*)
		Unclamp : BOOL; (*speed selection*)
		Clamp : BOOL; (*speed selection*)
	END_STRUCT;
	StackLight_typ : 	STRUCT 
		RedLight : BOOL;
		YellowLight : BOOL;
		GreenLight : BOOL;
	END_STRUCT;
END_TYPE
