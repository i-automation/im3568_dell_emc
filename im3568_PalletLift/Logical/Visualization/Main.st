
PROGRAM _INIT	 
END_PROGRAM

PROGRAM _CYCLIC
	Vis.Counter := Vis.Counter + 1;
	//display of limit switches
	IF Inputs.LeftActuator.PlusLimit THEN //AND Inputs.RightActuator.PlusLimit THEN
		Vis.PlusLimit := "GreenLED";
	ELSE
		Vis.PlusLimit := "GreyLED";
	END_IF;
	IF Inputs.LeftActuator.PlusWarn THEN //AND Inputs.RightActuator.PlusWarn THEN
		Vis.PlusWarn := "GreenLED";
	ELSE
		Vis.PlusWarn := "GreyLED";
	END_IF;
	IF Inputs.LeftActuator.MinusWarn THEN //AND Inputs.RightActuator.MinusWarn THEN
		Vis.MinusWarn := "GreenLED";
	ELSE
		Vis.MinusWarn := "GreyLED";
	END_IF;
	IF Inputs.LeftActuator.MinusLimit THEN //AND Inputs.RightActuator.MinusLimit THEN
		Vis.MinusLimit := "GreenLED";
	ELSE
		Vis.MinusLimit := "GreyLED";
	END_IF;
	//display of clamp/unclamp status
	IF Inputs.LeftActuator.Unclampled THEN //AND Inputs.RightActuator.Unclampled THEN
		Vis.Unclampled := 1;
	ELSE
		Vis.Unclampled := 0;
	END_IF;
	IF Inputs.LeftActuator.Clamped THEN //AND Inputs.RightActuator.Clamped THEN
		Vis.Clamped := 1;
	ELSE
		Vis.Clamped := 0;
	END_IF;
	//display of actuator statuses
	//display of safe status
	IF Inputs.LeftActuator.SafetyStatus THEN
		Vis.LeftSafeStatus := "GreenLED";
	ELSE
		Vis.LeftSafeStatus := "RedLED";
	END_IF;
	IF Inputs.RightActuator.SafetyStatus THEN
		Vis.RightSafeStatus := "GreenLED";
	ELSE
		Vis.RightSafeStatus := "RedLED";
	END_IF;
	//display of VFD status
	IF Inputs.LeftActuator.Fault THEN
		Vis.LeftVFDStatus := "GreenLED";
	ELSE
		Vis.LeftVFDStatus := "RedLED";
	END_IF;
	IF Inputs.RightActuator.Fault THEN
		Vis.RightVFDStatus := "GreenLED";
	ELSE
		Vis.RightVFDStatus := "RedLED";
	END_IF;
	//display of active status
	IF Inputs.LeftActuator.Active THEN
		Vis.LeftActive := "GreenLED";
	ELSE
		Vis.LeftActive := "GreyLED";
	END_IF;
	IF Inputs.RightActuator.Active THEN
		Vis.RightActive := "GreenLED";
	ELSE
		Vis.RightActive := "GreyLED";
	END_IF;
	//display of present pallet
	IF Inputs.PalletDetect THEN
		Vis.PalletPresent := "GreenLED";
	ELSE
		Vis.PalletPresent := "GreyLED";
	END_IF;
	//****************************************************************
	//******************display of IO statuses************************
	//****************************************************************
	//*********************Inputs*************************************
	//*********************Module 1*************************************
	//IN1
	IF Inputs.LeftActuator.MinusLimit THEN
		Vis.IO.In.IN1 := "GreenLED";
	ELSE
		Vis.IO.In.IN1 := "GreyLED";
	END_IF;
	//IN2
	IF Inputs.LeftActuator.MinusWarn THEN
		Vis.IO.In.IN2 := "GreenLED";
	ELSE
		Vis.IO.In.IN2 := "GreyLED";
	END_IF;
	//IN3
	IF Inputs.LeftActuator.PlusWarn THEN
		Vis.IO.In.IN3 := "GreenLED";
	ELSE
		Vis.IO.In.IN3 := "GreyLED";
	END_IF;
	//IN4
	IF Inputs.LeftActuator.PlusLimit THEN
		Vis.IO.In.IN4 := "GreenLED";
	ELSE
		Vis.IO.In.IN4 := "GreyLED";
	END_IF;
	//IN5
	IF Inputs.LeftActuator.Clamped THEN
		Vis.IO.In.IN5 := "GreenLED";
	ELSE
		Vis.IO.In.IN5 := "GreyLED";
	END_IF;
	//IN6
	IF Inputs.LeftActuator.SafetyStatus THEN
		Vis.IO.In.IN6 := "GreenLED";
	ELSE
		Vis.IO.In.IN6 := "GreyLED";
	END_IF;
	//IN7
	IF Inputs.RightActuator.SafetyStatus THEN
		Vis.IO.In.IN7 := "GreenLED";
	ELSE
		Vis.IO.In.IN7 := "GreyLED";
	END_IF;
	//IN8
	IF Inputs.LeftActuator.Unclampled THEN
		Vis.IO.In.IN8 := "GreenLED";
	ELSE
		Vis.IO.In.IN8 := "GreyLED";
	END_IF;
	//IN9
	IF Inputs.PalletDetect THEN
		Vis.IO.In.IN9 := "GreenLED";
	ELSE
		Vis.IO.In.IN9 := "GreyLED";
	END_IF;
	//IN10
	IF Inputs.LeftActuator.Active THEN
		Vis.IO.In.IN10 := "GreenLED";
	ELSE
		Vis.IO.In.IN10 := "GreyLED";
	END_IF;
	//IN11
	IF Inputs.LeftActuator.Fault THEN
		Vis.IO.In.IN11 := "GreenLED";
	ELSE
		Vis.IO.In.IN11 := "GreyLED";
	END_IF;
	//IN12  (Spare)
	Vis.IO.In.IN12 := "GreyLED";
	//*********************Module 2*************************************
	//IN13
	IF Inputs.RightActuator.MinusLimit THEN
		Vis.IO.In.IN13 := "GreenLED";
	ELSE
		Vis.IO.In.IN13 := "GreyLED";
	END_IF;
	//IN14
	IF Inputs.RightActuator.MinusWarn THEN
		Vis.IO.In.IN14 := "GreenLED";
	ELSE
		Vis.IO.In.IN14 := "GreyLED";
	END_IF;
	//IN15
	IF Inputs.RightActuator.PlusWarn THEN
		Vis.IO.In.IN15 := "GreenLED";
	ELSE
		Vis.IO.In.IN15 := "GreyLED";
	END_IF;
	//IN16
	IF Inputs.RightActuator.PlusLimit THEN
		Vis.IO.In.IN16 := "GreenLED";
	ELSE
		Vis.IO.In.IN16 := "GreyLED";
	END_IF;
	//IN17
	IF Inputs.RightActuator.Clamped THEN
		Vis.IO.In.IN17 := "GreenLED";
	ELSE
		Vis.IO.In.IN17 := "GreyLED";
	END_IF;
	//IN18
	IF Inputs.RightActuator.Unclampled THEN
		Vis.IO.In.IN18 := "GreenLED";
	ELSE
		Vis.IO.In.IN18 := "GreyLED";
	END_IF;
	//IN19	(Spare)
	Vis.IO.In.IN19 := "GreyLED";
	//IN20	(Spare)
	Vis.IO.In.IN20 := "GreyLED";
	//IN21	(Spare)
	Vis.IO.In.IN21 := "GreyLED";
	//IN22
	IF Inputs.RightActuator.Active THEN
		Vis.IO.In.IN22 := "GreenLED";
	ELSE
		Vis.IO.In.IN22 := "GreyLED";
	END_IF;
	//IN23
	IF Inputs.RightActuator.Fault THEN
		Vis.IO.In.IN23 := "GreenLED";
	ELSE
		Vis.IO.In.IN23 := "GreyLED";
	END_IF;
	//IN24	(Spare)
	Vis.IO.In.IN24 := "GreyLED";
	
	//*********************Outputs*************************************
	//OUT1
	IF Outputs.LeftActuator.DI1_StartStop THEN
		Vis.IO.Out.OUT1 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT1 := "GreyLED";
	END_IF;
	//OUT2
	IF Outputs.LeftActuator.DI2_ForwardReverse THEN
		Vis.IO.Out.OUT2 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT2 := "GreyLED";
	END_IF;
	//OUT3
	IF Outputs.LeftActuator.DI3_SpeedSelection THEN
		Vis.IO.Out.OUT3 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT3 := "GreyLED";
	END_IF;
	//OUT4
	IF Outputs.LeftActuator.DI4_SpeedSelection THEN
		Vis.IO.Out.OUT4 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT4 := "GreyLED";
	END_IF;
	//OUT5
	IF Outputs.LeftActuator.Unclamp THEN
		Vis.IO.Out.OUT5 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT5 := "GreyLED";
	END_IF;
	//OUT6
	IF Outputs.LeftActuator.Clamp THEN
		Vis.IO.Out.OUT6 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT6 := "GreyLED";
	END_IF;
	//OUT7
	Vis.IO.Out.OUT7 := "GreyLED";
	//OUT8
	Vis.IO.Out.OUT8 := "GreyLED";
	//OUT9
	IF Outputs.Enable THEN
		Vis.IO.Out.OUT9 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT9 := "GreyLED";
	END_IF;
	//OUT10
	IF Outputs.FaultReset THEN
		Vis.IO.Out.OUT10 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT10 := "GreyLED";
	END_IF;
	//OUT11
	IF Outputs.Mute1 THEN
		Vis.IO.Out.OUT11 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT11 := "GreyLED";
	END_IF;
	//OUT12
	IF Outputs.Mute2 THEN
		Vis.IO.Out.OUT12 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT12 := "GreyLED";
	END_IF;
	//*********************Module 2*************************************	
	//OUT13
	IF Outputs.RightActuator.DI1_StartStop THEN
		Vis.IO.Out.OUT13 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT13 := "GreyLED";
	END_IF;
	//OUT14
	IF Outputs.RightActuator.DI2_ForwardReverse THEN
		Vis.IO.Out.OUT14 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT14 := "GreyLED";
	END_IF;
	//OUT15
	IF Outputs.RightActuator.DI3_SpeedSelection THEN
		Vis.IO.Out.OUT15 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT15 := "GreyLED";
	END_IF;
	//OUT16
	IF Outputs.RightActuator.DI4_SpeedSelection THEN
		Vis.IO.Out.OUT16 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT16 := "GreyLED";
	END_IF;
	//OUT17
	IF Outputs.RightActuator.Unclamp THEN
		Vis.IO.Out.OUT17 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT17 := "GreyLED";
	END_IF;
	//OUT18
	IF Outputs.RightActuator.Clamp THEN
		Vis.IO.Out.OUT18 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT18 := "GreyLED";
	END_IF;
	//OUT19
	Vis.IO.Out.OUT19 := "GreyLED";
	//OUT20
	Vis.IO.Out.OUT20 := "GreyLED";
	//OUT21
	Vis.IO.Out.OUT21 := "GreyLED";
	//OUT22
	IF Outputs.Lights.RedLight THEN
		Vis.IO.Out.OUT22 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT22 := "GreyLED";
	END_IF;
	//OUT23
	IF Outputs.Lights.YellowLight THEN
		Vis.IO.Out.OUT23 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT23 := "GreyLED";
	END_IF;
	//OUT24
	IF Outputs.Lights.GreenLight THEN
		Vis.IO.Out.OUT24 := "GreenLED";
	ELSE
		Vis.IO.Out.OUT24 := "GreyLED";
	END_IF;
END_PROGRAM

PROGRAM _EXIT
END_PROGRAM

